﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Entities;
using Unity.Mathematics;
using Unity.Transforms;
using Unity.Collections;

public class PrimarchSpawner : MonoBehaviour {

    [SerializeField] private SpriteProps spriteProps;
    [SerializeField] private AnimationProps animationProps;

    private World defaultWorld;
    private EntityManager entityManager;

    private EntityArchetype entityArchetype;
    private Entity entity;

    void Awake () {
        defaultWorld = World.DefaultGameObjectInjectionWorld;
        entityManager = defaultWorld.EntityManager;

        entityArchetype = entityManager.CreateArchetype(
            typeof(SpriteTag),
            typeof(AnimatedSpriteTag),
            typeof(PrimarchTag),

            typeof(Translation),
            typeof(NonUniformScale),
            typeof(LocalToWorld),

            typeof(UVData),
            typeof(SpriteSharedData),
            typeof(SpriteAnimationData)
        );
    }

    void Start () {
        Spawn();
    }

    void Spawn() {
        entity = entityManager.CreateEntity(entityArchetype);
        entityManager.AddSharedComponentData(entity, new SpriteSharedData {
            mesh = spriteProps.mesh,
            material = spriteProps.material
        });
        entityManager.AddComponentData(entity, new Translation {
            Value = spriteProps.position
        });
        entityManager.AddComponentData(entity, new NonUniformScale {
            Value = spriteProps.scale
        });
        entityManager.AddComponentData(entity, new SpriteAnimationData {
            currentFrame = 0,
            frameCount = animationProps.animFrameCount,
            frameTimer = 0f,
            frameTimerCountdown = animationProps.animTimerCountdown
        });
    }
}
