﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Entities;
using Unity.Mathematics;
using Unity.Transforms;
using Unity.Collections;

public class PlayerSpawner : MonoBehaviour {

    [SerializeField] private SpriteProps spriteProps;
    [SerializeField] private AnimationProps animationProps;
    [SerializeField] private InputProps inputProps;
    [SerializeField] private MovementProps movementProps;

    private World defaultWorld;
    private EntityManager entityManager;

    private EntityArchetype playerArchetype;
    private Entity player;

    void Awake () {
        defaultWorld = World.DefaultGameObjectInjectionWorld;
        entityManager = defaultWorld.EntityManager;

        playerArchetype = entityManager.CreateArchetype(
            typeof(SpriteTag),
            typeof(AnimatedSpriteTag),
            typeof(PlayerTag),

            typeof(Translation),
            typeof(LocalToWorld),
            typeof(NonUniformScale),

            typeof(UVData),
            typeof(SpriteSharedData),
            typeof(SpriteAnimationData),
            typeof(InputData),
            typeof(MovementData),
            typeof(ActionData)
        );
    }

    void Start () {
        Spawn();
    }

    void Spawn() {
        player = entityManager.CreateEntity(playerArchetype);

        entityManager.AddSharedComponentData(player, new SpriteSharedData {
            mesh = spriteProps.mesh,
            material = spriteProps.material
        });
        entityManager.AddComponentData(player, new Translation {
            Value = spriteProps.position
        });
        entityManager.AddComponentData(player, new NonUniformScale {
            Value = spriteProps.scale
        });
        entityManager.AddComponentData(player, new SpriteAnimationData {
            currentFrame = 0,
            frameCount = animationProps.animFrameCount,
            frameTimer = 0f,
            frameTimerCountdown = animationProps.animTimerCountdown
        });
        entityManager.AddComponentData(player, new MovementData {
            speed = movementProps.speed,
            timeout = movementProps.timeout,
            treshold = movementProps.treshold,
            isTargetPosAchieved = true
        });
        entityManager.AddComponentData(player, new InputData {
            upKey = inputProps.upKey,
            rightKey = inputProps.rightKey,
            downKey = inputProps.downKey,
            leftKey = inputProps.leftKey
        });
    }
}
