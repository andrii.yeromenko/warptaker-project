﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Mathematics;

[System.Serializable]
public struct SpriteProps {
    public Mesh mesh;
    public Material material;
    public float3 position;
    public float3 scale;
}

[System.Serializable]
public struct AnimationProps {
    public int animFrameCount;
    public float animTimerCountdown;
}

[System.Serializable]
public struct InputProps {
    public KeyCode upKey;
    public KeyCode rightKey;
    public KeyCode downKey;
    public KeyCode leftKey;
}

[System.Serializable]
public struct MovementProps {
    public float speed;
    public float timeout;
    public float treshold;
}