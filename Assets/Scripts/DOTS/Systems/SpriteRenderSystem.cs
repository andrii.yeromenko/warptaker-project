﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Entities;
using Unity.Collections;
using Unity.Jobs;
using Unity.Burst;
using Unity.Transforms;
using Unity.Mathematics;

[UpdateAfter(typeof(SpriteAnimationSystem))]
public class SpriteRendererSystem : SystemBase
{
    MaterialPropertyBlock materialPropertyBlock = new MaterialPropertyBlock();
    protected override void OnUpdate () {

        Entities.WithAll<SpriteTag>().ForEach((ref LocalToWorld matrix, in Translation translation, in NonUniformScale scale) => {
            float3 position = translation.Value;
            position.z = position.y * .01f;
            matrix.Value = Matrix4x4.TRS(position, Quaternion.identity, scale.Value);
        }).ScheduleParallel();

        Entities.WithAll<SpriteTag>().WithoutBurst().ForEach(
            (Entity entity, SpriteSharedData spriteSharedData, ref Translation translation, in UVData uvData, in LocalToWorld matrix) => {
                materialPropertyBlock.SetVectorArray("_MainTex_ST", new Vector4[] {uvData.uv});
                Graphics.DrawMesh(
                    spriteSharedData.mesh,
                    matrix.Value,
                    spriteSharedData.material,
                    0, //Layer
                    Camera.main,
                    0, //Submesh
                    materialPropertyBlock
                );
            }
        ).Run();
    }
}
