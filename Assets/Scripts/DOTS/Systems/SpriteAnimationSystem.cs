﻿using UnityEngine;
using Unity.Entities;
using Unity.Jobs;
using Unity.Transforms;
using Unity.Mathematics;

public class SpriteAnimationSystem: SystemBase {

    protected override void OnUpdate () {
        float deltaTime = Time.DeltaTime;

        Entities.WithAll<AnimatedSpriteTag>().ForEach((ref SpriteAnimationData spriteAnimationData, ref UVData uvData, in Translation translation) => {
            spriteAnimationData.frameTimer += deltaTime;
            while (spriteAnimationData.frameTimer >= spriteAnimationData.frameTimerCountdown) {
                spriteAnimationData.frameTimer = 0;
                spriteAnimationData.currentFrame = (spriteAnimationData.currentFrame + 1) % spriteAnimationData.frameCount;

                float uvHeight = 1f/ spriteAnimationData.frameCount;
                float uvOffsetY = uvHeight * spriteAnimationData.currentFrame;
                uvData.uv = new Vector4(1, uvHeight, 0, uvOffsetY);
            }
        }).ScheduleParallel();
    }
}
