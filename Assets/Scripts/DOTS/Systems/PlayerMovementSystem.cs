﻿using UnityEngine;
using Unity.Entities;
using Unity.Mathematics;
using Unity.Transforms;
using System;

public class PlayerMovementSystem : SystemBase
{
    EndSimulationEntityCommandBufferSystem m_EndSimulationEcbSystem;

    protected override void OnCreate()
    {
        base.OnCreate();
        // Find the ECB system once and store it for later usage
        m_EndSimulationEcbSystem = World
            .GetOrCreateSystem<EndSimulationEntityCommandBufferSystem>();
    }

    protected override void OnUpdate() {
        float deltaTime = Time.DeltaTime;
        var ecb = m_EndSimulationEcbSystem.CreateCommandBuffer().ToConcurrent();

        Entities.WithAll<PlayerTag>().ForEach((ref MovementData movementData, in Translation pos) => {
            movementData.isTargetPosAchieved = movementData.targetPosition.x == pos.Value.x && movementData.targetPosition.y == pos.Value.y;
        }).Run();

        Entities.WithAll<PlayerTag>().ForEach((ref ActionData actionData, ref MovementData movementData, in Translation pos) => {
            if (actionData.timer <= 0 && movementData.isMove && movementData.isTargetPosAchieved) {
                movementData.targetPosition += movementData.direction;
                actionData.timer = movementData.timeout;
            }
        }).Run();

        Entities.WithAll<PlayerTag>().ForEach((ref Translation pos, in MovementData movementData) => {
            float newX = pos.Value.x;
            float newY = pos.Value.y;

            if(math.abs(pos.Value.x - movementData.targetPosition.x) > movementData.treshold) {
                newX = math.lerp(pos.Value.x, movementData.targetPosition.x, deltaTime * movementData.speed);
            }
            else {
                newX = movementData.targetPosition.x;
            }

            if(math.abs(pos.Value.y - movementData.targetPosition.y) > movementData.treshold) {
                newY = math.lerp(pos.Value.y, movementData.targetPosition.y, deltaTime * movementData.speed);
            }
            else {
                newY = movementData.targetPosition.y;
            }

            pos.Value = new float3(newX, newY, pos.Value.z);
        }).Run();

        m_EndSimulationEcbSystem.AddJobHandleForProducer(this.Dependency);
    }
}
