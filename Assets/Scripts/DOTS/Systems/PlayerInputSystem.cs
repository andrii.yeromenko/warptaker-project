﻿using Unity.Entities;
using System;
using UnityEngine;

public class PlayerInputSystem : SystemBase
{
    protected override void OnUpdate() {
        Entities.ForEach((ref MovementData moveData, in InputData inputData) => {
            bool movementRight = Input.GetKey(inputData.rightKey);
            bool movementLeft = Input.GetKey(inputData.leftKey);
            bool movementUp = Input.GetKey(inputData.upKey);
            bool movementDown = Input.GetKey(inputData.downKey);

            moveData.direction.x = movementRight ? 1 : 0;
            moveData.direction.x -= movementLeft ? 1 : 0;
            moveData.direction.y = movementUp ? 1 : 0;
            moveData.direction.y -= movementDown ? 1 : 0;

            moveData.isMove = movementRight || movementLeft || movementUp || movementDown;
        }).Run();
    }
}
