﻿using Unity.Entities;

public class PlayerActionSystem : SystemBase
{
    protected override void OnUpdate() {
        float deltaTime = Time.DeltaTime;

        Entities.WithAll<PlayerTag>().ForEach((ref ActionData actionData) => {
            if(actionData.timer <= 0) {
                actionData.timer = 0;
            }
            else {
                actionData.timer -= deltaTime;
            }
        }).Run();
    }
}
