﻿using Unity.Entities;

public struct SpriteAnimationData: IComponentData {
    public int currentFrame;
    public int frameCount;
    public float frameTimer;
    public float frameTimerCountdown;
}