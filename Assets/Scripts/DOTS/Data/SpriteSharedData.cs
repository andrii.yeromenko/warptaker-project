﻿using System.Collections;
using System;
using System.Collections.Generic;
using UnityEngine;
using Unity.Entities;

[System.Serializable]
public struct SpriteSharedData : ISharedComponentData, IEquatable<SpriteSharedData> {
    public Mesh mesh;
    public Material material;

    public bool Equals(SpriteSharedData obj)
    {
        return mesh == obj.mesh && material == obj.material;
    }

    public override int GetHashCode() {
        int hash = mesh.GetHashCode();
        if(ReferenceEquals(material, null)) {
            hash ^= material.GetHashCode();
        }

        return hash;
    }
}
