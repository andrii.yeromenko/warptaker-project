﻿using Unity.Entities;
using UnityEngine;

public struct UVData : IComponentData
{
    public Vector4 uv;
}
