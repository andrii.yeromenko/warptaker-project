﻿using Unity.Entities;
using Unity.Mathematics;

public struct MovementData : IComponentData
{
    public float3 targetPosition;
    public float3 direction;

    public bool isMove;
    public bool isTargetPosAchieved;

    public float speed;
    public float timeout;
    public float treshold;
}
