﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Entities;

public struct ParticleComponent : IComponentData {
    public float lifeTime;
}
