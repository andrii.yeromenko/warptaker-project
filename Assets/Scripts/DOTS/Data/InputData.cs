﻿using UnityEngine;
using Unity.Entities;

public struct InputData : IComponentData
{
    public KeyCode upKey;
    public KeyCode rightKey;
    public KeyCode downKey;
    public KeyCode leftKey;
}
